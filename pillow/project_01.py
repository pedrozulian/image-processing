# Por conta do desenvolvimento do pillow ter sido interrompido,
# as atualizações passaram a ser feitas no PIL (Python Image Library)
import PIL
from PIL import Image
import numpy as np
from matplotlib import image
from matplotlib import pyplot

## Pego a algumas informações sobre a imagem ##

# image = Image.open("images/area-rural-arada.png")
# print(image)
# print(image.format)
# print(image.mode)
# print(image.size)

## Pegando matriz referente a imagem carregada ##
data = image.imread("images/area-rural-arada.png")
print(int(data))
print(data.dtype)

